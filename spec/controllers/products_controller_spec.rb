require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "#show" do
    let!(:product) { create(:product) }

    before do
      get :show, params: { id: product.id }
    end

    it "正常にレスポンスを返す" do
      expect(response).to be_successful
    end

    it "正常にテンプレートを返す" do
      expect(response).to render_template(:show)
    end

    it "@productが正しい値を持つ" do
      expect(assigns(:product)).to eq product
    end
  end
end
